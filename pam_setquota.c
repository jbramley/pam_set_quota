#include <unistd.h>             
#include <sys/param.h>          
#include <errno.h>              
#include <syslog.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <pwd.h>
#include <grp.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <sys/stat.h>
#include <linux/quota.h>
#include <security/pam_appl.h>
#include <security/pam_modules.h>
#include <security/pam_ext.h>


#define POSTGRAD 806;
#define STAFF 800;

PAM_EXTERN int pam_sm_open_session (pam_handle_t *pamh, int flags, int argc, const char **argv)
{
	int retval;
	int postgrad = POSTGRAD;
	int staff = STAFF;
	const char* user;
	const struct passwd *pwd;
	struct if_dqblk nhdq,nddq;
	struct if_dqblk chdq,cddq;
	int hhl,dhl,hsl,dsl;
	const char* homedevice = "/dev/mapper/edr9871-home";
	const char* datadevice = "/dev/mapper/edr9871-data";
	struct group *grp;
        char **members;
	bool dataQuotaSet = false;
	bool homeQuotaSet = false;

	retval = pam_get_user(pamh,&user,NULL);



	if (retval != PAM_SUCCESS || user == NULL || *(const char *)user == '\0') {
		return PAM_USER_UNKNOWN;
	}

	pwd = getpwnam(user);

        if (pwd == NULL) {
                return PAM_CRED_INSUFFICIENT;
        }


	if ( (quotactl(QCMD(Q_GETQUOTA,USRQUOTA),homedevice,pwd->pw_uid,(void *) &chdq) == -1) 
			|| (quotactl(QCMD(Q_GETQUOTA,USRQUOTA),datadevice,pwd->pw_uid,(void *) &cddq) == -1)){
		
		syslog(LOG_ERR,"Unable to get current quotas for %s", pwd->pw_name);
                return PAM_PERM_DENIED;
        }

	hsl = chdq.dqb_bsoftlimit;
        hhl = chdq.dqb_bhardlimit;
        dsl = cddq.dqb_bsoftlimit;
        dhl = cddq.dqb_bhardlimit;


	//dont want to effect system users	
	if( pwd->pw_uid > 1000 ) {
		setgrent();
		while(( grp = getgrent()) != NULL) {
			//account groups for postgrad .. firstyr are all bellow 900
			if(grp->gr_gid < 900 ){
				members = grp->gr_mem;
				while(*members){
					if(strcmp(*members,pwd->pw_name) == 0 ) {
						if(grp->gr_gid == staff || grp->gr_gid == postgrad) {
												
							if ( hsl == 0 && hhl == 0 ){	        
						 		homeQuotaSet = true;
								nhdq.dqb_bsoftlimit = 5000000;
                                                 		nhdq.dqb_bhardlimit = 5500000;
                                                 		nhdq.dqb_isoftlimit = 0;
                                                 		nhdq.dqb_ihardlimit = 0;
							}

							 if (dsl == 0 && dhl == 0 ) {
								dataQuotaSet = true;
								//seperate between data and home	
								nddq.dqb_bsoftlimit = 1000000;
                                                        	nddq.dqb_bhardlimit = 20000000;
                                                        	nddq.dqb_isoftlimit = 0;
                                                        	nddq.dqb_ihardlimit = 0;
							}				
	
						}
						else{

							if ( hsl == 0 && hhl == 0 ){
								homeQuotaSet = true;
								nhdq.dqb_bsoftlimit = 1000000;
                                                		nhdq.dqb_bhardlimit = 1500000;
                                                		nhdq.dqb_isoftlimit = 0;
								nhdq.dqb_ihardlimit = 0;
							}

							if (dsl == 0 && dhl == 0 ) {
                                                                dataQuotaSet = true;
								nddq.dqb_bsoftlimit = 1000000;
                                                        	nddq.dqb_bhardlimit = 15000000;
                                                        	nddq.dqb_isoftlimit = 0;
                                                        	nddq.dqb_ihardlimit = 0;
							}
							
						}
					}
					members++;	
				}
			}
		}	

		endgrent();


		if ( homeQuotaSet == true ) {
			nhdq.dqb_valid  |= QIF_BLIMITS;
	                nhdq.dqb_valid  |= QIF_ILIMITS;

			if( quotactl(QCMD(Q_SETQUOTA,USRQUOTA),homedevice,pwd->pw_uid,(void *) &nhdq) == -1 ){
	                      	 syslog(LOG_ERR,"Unable to set quota on home for %s", pwd->pw_name);
				 return PAM_PERM_DENIED;
               		}
		}

		if ( dataQuotaSet) {
			nddq.dqb_valid  |= QIF_BLIMITS;
	                nddq.dqb_valid  |= QIF_ILIMITS;

			if( quotactl(QCMD(Q_SETQUOTA,USRQUOTA),datadevice,pwd->pw_uid,(void *) &nddq) == -1 ){
                                 syslog(LOG_ERR,"Unable to set quota on data for %s", pwd->pw_name);
                                 return PAM_PERM_DENIED;
                	}

		}
		
	}

	
	return PAM_SUCCESS;


}



/* Ignore */
PAM_EXTERN int pam_sm_close_session (pam_handle_t *pamh,  int flags, int argc, const char **argv)
{
	   return PAM_SUCCESS;
}
